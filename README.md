# CRM built on Rails for Self Study
# Add user
rake db:migrate && rake ffcrm:setup:admin USERNAME=admin PASSWORD=password EMAIL=admin@example.com

# CSS
app/assets/stylesheets
# javascripts
app/assets/javascripts
# languages
config/locales/
# search
app/views/entities/search
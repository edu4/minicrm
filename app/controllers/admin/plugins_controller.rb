
#
#

#------------------------------------------------------------------------------
class Admin::PluginsController < Admin::ApplicationController
  before_filter "set_current_tab('admin/plugins')", :only => [ :index ]

  # GET /admin/plugins
  # GET /admin/plugins.xml
  #----------------------------------------------------------------------------
  def index
    @plugins = MiniCRM::Plugin.list

    respond_with(@plugins)
  end
end


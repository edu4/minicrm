
#
#

#------------------------------------------------------------------------------
class Admin::SettingsController < Admin::ApplicationController
  before_filter "set_current_tab('admin/settings')", :only => [ :index ]

  # GET /admin/settings
  # GET /admin/settings.xml
  #----------------------------------------------------------------------------
  def index
  end
end


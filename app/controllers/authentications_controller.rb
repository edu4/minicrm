class AuthenticationsController < ApplicationController

  before_filter :require_no_user, :only => [ :new, :create, :show ]
  before_filter :require_user, :only => :destroy

  #----------------------------------------------------------------------------
  def new
    @authentication = Authentication.new
  end

  #----------------------------------------------------------------------------
  def show
    redirect_to login_url
  end

  #----------------------------------------------------------------------------
  # create session
  def create
    @authentication = Authentication.new(params[:authentication])

    if @authentication.save && !@authentication.user.suspended?
      # can flash notice here fo
      flash[:notice] = "logged in"

      redirect_back_or_default root_url
    else
      if @authentication.user && @authentication.user.awaits_approval?
        flash[:notice] = t(:msg_account_not_approved)
      else
        flash[:warning] = t(:msg_invalig_login)
      end
      redirect_to :action => :new
    end
  end


  #----------------------------------------------------------------------------
  alias :update :create

  #----------------------------------------------------------------------------
  
  #log out and destroy current session
  def destroy
    current_user_session.destroy
    redirect_back_or_default login_url
  end

end


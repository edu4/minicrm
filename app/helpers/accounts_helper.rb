
#
#

#------------------------------------------------------------------------------
module AccountsHelper

  # Sidebar checkbox control for filtering accounts by category.
  #----------------------------------------------------------------------------
  def account_category_checkbox(category, count)
    entity_filter_checkbox(:category, category, count)
  end

  # Quick account summary for RSS/ATOM feeds.
  #----------------------------------------------------------------------------
  def account_summary(account)
    [ number_to_currency(account.opportunities.pipeline.map(&:weighted_amount).sum, :precision => 0),
      t(:added_by, :time_ago => time_ago_in_words(account.created_at), :user => account.user_id_full_name),
      t('pluralize.opportunity', account.opportunities.count),
      t('pluralize.comment', account.comments.count)
    ].join(', ')
  end

  # Generates a select list with the first 25 accounts
  # and prepends the currently selected account, if any.
  #----------------------------------------------------------------------------
  def account_select(options = {})
      options[:selected] = (@account && @account.id) || 0
      accounts = ([@account] + Account.my.order(:name).limit(25)).compact.uniq
      collection_select :account, :id, accounts, :id, :name, options,
                        {:"data-placeholder" => t(:select_an_account),
                         :"data-url" => auto_complete_accounts_path(format: 'json'),
                         :style => "width:330px; display:none;",
                         :class => 'ajax_chosen' }
  end

  # Select an existing account or create a new one.
  #----------------------------------------------------------------------------
  def account_select_or_create(form, &block)
    options = {}
    yield options if block_given?

    content_tag(:div, :class => 'label') do
      t(:account).html_safe +

      content_tag(:span, :id => 'account_create_title') do
        "(#{t :create_new} #{t :or} <a href='#' onclick='crm.select_account(); return false;'>#{t :select_existing}</a>):".html_safe
      end +

      content_tag(:span, :id => 'account_select_title') do
        "(<a href='#' onclick='crm.create_account(); return false;'>#{t :create_new}</a> #{t :or} #{t :select_existing}):".html_safe
      end +

      content_tag(:span, ':', :id => 'account_disabled_title')
    end +

    account_select(options) +
    form.text_field(:name, :style => 'width:324px; display:none;')
  end

  # Output account url for a given ct
  # - a helper so it is easy to override in plugins that allow for several accounts


end

class AccountContact < ActiveRecord::Base
  belongs_to :account


  validates_presence_of :account_id

  ActiveSupport.run_load_hooks(:fat_free_crm_account_contact, self)
end


#
#

#------------------------------------------------------------------------------
class OpportunityObserver < ActiveRecord::Observer
  observe :opportunity

  @@opportunities = {}

  def after_create(item)
    if  item.stage == "won"
      #todo
    end
  end

  def before_update(item)
    @@opportunities[item.id] = Opportunity.find(item.id).freeze
  end

  def after_update(item)
    original = @@opportunities.delete(item.id)
    if original
      if original.stage != "won" && item.stage == "won"
        item.update_attribute(:probability, 100) # Set probability to 100% if won
        return log_activity(item, :won)


      elsif original.stage != "lost" && item.stage == "lost"
        item.update_attribute(:probability, 0)   # Set probability to 0% if lost
      end
    end
  end

  private

  def log_activity(item, event)
    item.send(item.class.versions_association_name).create(:event => event, :whodunnit => PaperTrail.whodunnit)
  end



  ActiveSupport.run_load_hooks(:fat_free_crm_opportunity_observer, self)
end

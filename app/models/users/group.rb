
#
#

#------------------------------------------------------------------------------
class Group < ActiveRecord::Base
  has_and_belongs_to_many :users
  has_many :permissions

  attr_accessible :name, :user_ids

  validates :name, :presence => true, :uniqueness => true

  ActiveSupport.run_load_hooks(:fat_free_crm_group, self)
end

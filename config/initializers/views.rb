
#
#

#------------------------------------------------------------------------------

# Register the views that MiniCRM provides
#------------------------------------------------------------------------------

[{:name => 'opportunities_index_brief', :title => 'Brief format',
  :controllers => ['opportunities'], :actions => ['index'], :template => 'opportunities/index_brief'},
 {:name => 'opportunities_index_long', :title => 'Long format', :icon => 'fa-list',
   :controllers => ['opportunities'], :actions => ['index'], :template => 'opportunities/index_long'}, # default
 {:name => 'opportunities_show_normal', :title => 'Normal format', :icon => 'fa-list',
   :controllers => ['opportunities'], :actions => ['show'], :template => nil}, # default show view

 {:name => 'accounts_index_brief', :title => 'Brief format', :icon => 'fa-bars',
  :controllers => ['accounts'], :actions => ['index'], :template => 'accounts/index_brief'}, # default
 {:name => 'accounts_index_long', :title => 'Long format', :icon => 'fa-list',
  :controllers => ['accounts'], :actions => ['index'], :template => 'accounts/index_long'}, # default
 {:name => 'accounts_show_normal', :title => 'Normal format', :icon => 'fa-list',
   :controllers => ['accounts'], :actions => ['show'], :template => nil}, # default show view

 {:name => 'leads_index_brief', :title => 'Brief format', :icon => 'fa-bars',
  :controllers => ['leads'], :actions => ['index'], :template => 'leads/index_brief'}, # default
 {:name => 'leads_index_long', :title => 'Long format', :icon => 'fa-list',
  :controllers => ['leads'], :actions => ['index'], :template => 'leads/index_long'},
 {:name => 'leads_show_normal', :title => 'Normal format', :icon => 'fa-list',
   :controllers => ['leads'], :actions => ['show'], :template => nil}, # default show view

 {:name => 'campaigns_index_brief', :title => 'Brief format', :icon => 'fa-bars',
  :controllers => ['campaigns'], :actions => ['index'], :template => 'campaigns/index_brief'}, # default
 {:name => 'campaigns_index_long', :title => 'Long format', :icon => 'fa-list',
  :controllers => ['campaigns'], :actions => ['index'], :template => 'campaigns/index_long'},
 {:name => 'campaigns_show_normal', :title => 'Normal format', :icon => 'fa-list',
   :controllers => ['campaigns'], :actions => ['show'], :template => nil}, # default show view

].each {|view| MiniCRM::ViewFactory.new(view)}


#
#

#------------------------------------------------------------------------------
# Mini CRM settings are stored in three places, and are loaded in the following order:
#
# 1) config/settings.default.yml
# 2) config/settings.yml  (if exists)
# 3) 'settings' table in database  (if exists)
#
# Any settings in `config/settings.yml` will override those in
# `config/settings.default.yml`, and settings in the database table have the highest priority.
#
# This settings arrangement offers a lot of flexibility:
#
# * Developers can add new settings to the application with default values,
#   without requiring the user to manually update setting files or run tasks.
# * No manual configuration is required to start the application after installation
# * IT administrators can change the default configuration by adding 'settings.yml'
#   to their git repo, or by running SQL that updates the 'settings' table in the database.
# * Admin users can change settings via the admin interface, without needing to edit YAML files.
#
# PLEASE DO NOT EDIT THE SETTINGS IN THIS FILE.
# COPY THE SETTINGS THAT YOU WANT TO OVERRIDE INTO `config/settings.yml`
#
#------------------------------------------------------------------------------


# Host
#------------------------------------------------------------------------------
# Host for URLs when action mailer generates emails.
#
# When sending email, you will need to set the host name so that the mailer can
# generate urls inside emails that point back to your site. Unlike controllers
# from Action Pack, the mailer instance doesn’t have any context about the
# incoming request, so you’ll need to provide all of the details needed to
# generate a URL.
#
# :host: "www.example.com"
#
:host: ""


# Base URL
#------------------------------------------------------------------------------
# Leave base_url blank if Mini CRM has been deployed in root URL, otherwise
# specify subdirectory. For example, when deployed to:
#
# http://www.domain.com       => :base_url: ""       # (leave blank)
# http://crm.domain.com       => :base_url: ""       # (leave blank)
# http://www.domain.com/crm   => :base_url: "/crm"   # (without trailing slash)
# http://www.domain.com/a/crm => :base_url: "/a/crm" # (without trailing slash)
#
:base_url: ""


# Locale
#------------------------------------------------------------------------------
# Default locale is American English.
#
:locale: "en-US"


# Settings for outgoing email (SMTP)
# Uncomment to set up for your specific mailer
#------------------------------------------------------------------------------
#:smtp:
  #:address                    : ""        # e.g. smtp.gmail.com
  #:from                       : ""        # e.g. no-reply@your-domain.com
  #:enable_starttls_auto       : true
  #:port                       : "587"
  #:authentication             : :plain
  #:user_name                  : ""
  #:password                   : ""





# Settings for 'Reply via email' inbox (IMAP)
#------------------------------------------------------------------------------
:email_comment_replies:
  :server                     : ""        # IMAP server name.
  :port                       : ""        # IMAP server port number.
  :ssl                        : ""        # True for secure IMAP connection, false othewise.
  :address                    : ""        # Comment reply email address.
  :user                       : ""        # User to login to the IMAP server.
  :password                   : ""        # Password to login to the IMAP server.
  :scan_folder                : ""        # Folder to scan for comment replies.
#  :move_to_folder            : ""        # Optional: folder to move processed messages to.
#  :move_invalid_to_folder    : ""        # Optional: folder to move invalid messages to.


# Individual locales per user
#------------------------------------------------------------------------------
# To enable language selection in User/Profile set the following option to true.
# By default, individual users can't pick their own locale and are presented
# with the default locale.
#
:per_user_locale: false


# Default records permissions
#------------------------------------------------------------------------------
# Set the defaults permission for new records. Possible values are:
#   Private -- private access, only for the creator of the record.
#   Public  -- public access, share with everyone. This is the default.
#   Shared  -- shared with selected accounts, should be selected by the user.
#
:default_access: "Public"


# User Signup
#------------------------------------------------------------------------------
# The following setting controls user signup. Possible values are:
#   :allowed        -- users can sign up and use the system once they have registered.
#   :not_allowed    -- users can only be added by system administrator. This is the default.
#   :needs_approval -- users can sign up but must be approved before they can use the system.
#
:user_signup: :not_allowed


:compound_address: true


# Dropdown calendar format
#------------------------------------------------------------------------------
# The following setting controls whether to show time in dropdown calendar
# when scheduling a task for specific date.
#   true  -- allow selecting date and time.  (default)
#   false -- allow selecting date only.
#
:task_calendar_with_time: true


# Enable/disable first/last name validations on Leads and ct
#------------------------------------------------------------------------------
#
:require_first_names: true
:require_last_names:  true

# Enable/disable name validation on Accounts
:require_unique_account_names: true

# Show comments on dashboard
:comments_visible_on_dashboard: false

# Background information
:background_info: []

# Priority countries
#------------------------------------------------------------------------------
# Specify which countries (if any) should appear at the top of country pickers
# priority_countries:
#  - Burkina Faso

# Main and Admin Tabs
#------------------------------------------------------------------------------
#tasks icon is :fa-check-square-o
:tabs:
- :active: true
  :text: :tab_leads
  :icon: :fa-user
  :url:
    :controller: leads
- :active: false
  :text: :tab_accounts
  :icon: :fa-users
  :url:
    :controller: accounts
- :active: false
  :text: :tab_opportunities
  :icon: :fa-money
  :url:
    :controller: opportunities


:admin_tabs:
- :active: true
  :text: :admin_tab_users
  :icon: :fa-user
  :url:
    :controller: admin/users
- :active: true
  :text: :admin_tab_groups
  :icon: :fa-users
  :url:
    :controller: admin/groups
- :active: false
  :text: :admin_tab_fields
  :icon: :fa-list-alt
  :url:
    :controller: admin/fields
- :active: false
  :text: :admin_tab_tags
  :icon: :fa-tags
  :url:
    :controller: admin/tags


account_category:


account_category_color:
  :affiliate   : lavender
  :competitor  : paleturquoise
  :customer    : pink
  :partner     : bisque
  :reseller    : thistle
  :vendor      : skyblue

# Campaign Status. To add custom status use string value, for example:
#
# campaign_status:
#   - "Custom"
#
#------------------------------------------------------------------------------
campaign_status:
  - :planned
  - :started
  - :completed
  - :on_hold
  - :called_off

campaign_status_color:
  :planned    : Gold
  :started    : Turquoise
  :completed  : Lime
  :on_hold    : Orchid
  :called_off : OrangeRed

# Lead Status and Source.
#------------------------------------------------------------------------------
lead_status:
  - :called
  - :met
  - :first_interview
  - :second_interview
  - :third_interview
  - :final_interview
  - :rejected
  - :offered

lead_status_color:
  :called    : Silver
  :met       : Turquoise
  :first_interview : PaleGoldenRod
  :second_interview : PaleGoldenRod
  :third_interview : skyblue
  :final_interview : Orchid
  :converted : Lime
  :rejected  : Red
  :offered   : Lime

lead_source:
  - :campaign
  - :cold_call
  - :conference
  - :online
  - :referral
  - :self
  - :web
  - :word_of_mouth
  - :other

# Opportunity Stage.
#------------------------------------------------------------------------------
opportunity_default_stage: :prospecting
opportunity_stage:
  - :called
  - :met
  - :first_interview
  - :second_interview
  - :third_interview
  - :final_interview
  - :rejected
  - :offered

opportunity_stage_color:
  :called  : Silver
  :analysis     : PaleGoldenRod
  :presentation : Salmon
  :proposal     : Gold
  :negotiation  : Orchid
  :final_review : Turquoise
  :won          : Lime
  :lost         : OrangeRed





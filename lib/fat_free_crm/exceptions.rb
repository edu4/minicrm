
#
#

#------------------------------------------------------------------------------
module MiniCRM
  class MissingSettings < StandardError; end
  class ObsoleteSettings < StandardError; end
end

class ActionController::Base
  rescue_from MiniCRM::MissingSettings,  :with => :render_fat_free_crm_exception
  rescue_from MiniCRM::ObsoleteSettings, :with => :render_fat_free_crm_exception

  private

  def render_fat_free_crm_exception(exception)
    logger.error exception.inspect
    render :layout => false, :template => "/layouts/500", :format => :html, :status => 500, :locals => { :exception => exception }
  end
end


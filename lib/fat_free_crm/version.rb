
#
#

#------------------------------------------------------------------------------
module MiniCRM
  module VERSION #:nodoc:
    MAJOR = 0
    MINOR = 13
    TINY  = 6
    PRE   = nil

    STRING = [MAJOR, MINOR, TINY, PRE].compact.join('.')
  end
end
